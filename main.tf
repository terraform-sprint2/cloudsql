resource "google_sql_database_instance" "instance" {
  name             = "${var.name}-instance"
  region           = var.region
  database_version = var.versionn
  settings {
    tier = var.tier
  }

  deletion_protection  = var.deletion_protection

}

resource "google_sql_database" "database" {
  name     = "${var.name}-database"
  instance = google_sql_database_instance.instance.name
}


resource "google_sql_user" "users" {
  name     =  "${var.name}-user"
  instance = google_sql_database_instance.instance.name
  host     = "${var.name}-user.com"
  password = var.password
}
